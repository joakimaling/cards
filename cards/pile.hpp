#pragma once

#include "card.hpp"
#include <algorithm>
#include <chrono>
#include <deque>
#include <ostream>
#include <random>
#include <stdexcept>

namespace cards {
	class Pile {
		protected:
			std::deque<Card> cards;

		public:
			/**
			 * Adds a given card to the pile. It will be put in the back.
			 *
			 * @param card The card to be added to the pile
			 */
			void putBottom(const Card& card) {
				cards.push_back(card);
			}

			/**
			 * Adds a given card to the pile. It will be put in the front.
			 *
			 * @param card The card to be added to the pile
			 */
			void putTop(const Card& card) {
				cards.push_front(card);
			}

			/**
			 * Draws a card from the pile and returns it.
			 *
			 * @return The drawn card
			 *
			 * @throws std::length_error
			 */
			Card draw() {
				if (isEmpty()) {
					throw std::length_error("No more cards to draw");
				}

				Card card = cards.front();
				cards.pop_front();
				return card;
			}

			/**
			 * Flips the card with the given index in the pile.
			 *
			 * @param index The index of the card
			 */
			void flip(const std::size_t index) {
				cards.at(index).flip();
			}

			/**
			 * Flips all the cards in the pile.
			 */
			void flip() {
				for (Card& card: cards) {
					card.flip();
				}
			}

			/**
			 * [getCards description]
			 * @return [description]
			 */
			std::deque<Card> getCards() const {
				return cards;
			}

			/**
			 * Returns the number of cards currently in the pile.
			 *
			 * @return Number of cards in the pile
			 */
			std::size_t getSize() const {
				return cards.size();
			}

			/**
			 * Returns true if the pile is empty, i.e. if there're no cards left
			 * in it.
			 *
			 * @return True if empty, false if not
			 */
			bool isEmpty() const {
				return cards.empty();
			}

			/**
			 * Moves all the cards to the back of the given pile.
			 *
			 * @param pile The pile on to which to move the cards
			 */
			void moveTo(Pile& pile) {
				pile.cards.insert(pile.cards.end(), cards.begin(), cards.end());
				cards.clear();
			}

			/**
			 * Shuffles the pile of cards randomly.
			 */
			void shuffle() {
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				std::shuffle(cards.begin(), cards.end(), std::default_random_engine(seed));
			}

			/**
			 * Sorts the pile of cards.
			 */
			void sort() {
				std::sort(cards.begin(), cards.end());
			}
	};

	/**
	 * Used to print the string representation of the cards in the pile using
	 * streams.
	 */
	std::ostream& operator<<(std::ostream& os, const Pile& pile) {
		for (const Card& card: pile.getCards()) {
			os << card << " ";
		}

		return os;
	}
}
