#pragma once

#include "hand.hpp"
#include "pile.hpp"
#include <stdexcept>
#include <vector>

namespace cards {
	const unsigned DECK_SIZE(52);

	class Deck: public Pile {
		public:
			Deck() {
				for (unsigned i = 0; i < DECK_SIZE; i++) {
					cards.emplace_back(i);
				}
			}

			/**
			 * Deals a number of cards to the given hands. The quantity deter-
			 * mines how many cards are dealt to each hand. If there're too few
			 * cards to deal an exception is thrown.
			 *
			 * @param hands    A vector of hands to deal cards to
			 * @param quantity The number of cards dealt to each hand
			 *
			 * @throws std::length_error
			 */
			void deal(std::vector<Hand>& hands, const unsigned quantity = 1) {
				if (getSize() < hands.size() * quantity) {
					throw std::length_error("Not enough cards to deal");
				}

				for (unsigned i = 0; i < quantity; i++) {
					for (Hand& hand: hands) {
						hand.add(draw());
					}
				}
			}
	};
}
