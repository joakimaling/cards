#pragma once

#include "pile.hpp"
#include <stdexcept>
#include <string>
#include <vector>

namespace cards {
	const unsigned HAND_SIZE(5);

	class Hand: public Pile {
		private:
			std::string name;
			unsigned size;

		public:
			Hand(const std::string name, const unsigned size = 0): Pile(), name(name), size(size) {}

			/**
			 * Adds a given card to the hand. If the hand is considered full, an
			 * exception will be emitted.
			 *
			 * @param card The card to be added to the hand
			 *
			 * @throws std::length_error
			 */
			void add(const Card& card) {
				if (size > 0 && size > getSize() + 1) {
					throw std::length_error("Hand " + getName() + " is full");
				}

				Pile::putTop(card);
			}

			/**
			 * Discards the given, pointed out by indices, cards from the hand
			 * to a pile of cards.
			 *
			 * @param pile    A pile of cards on which to put the discarded ones
			 * @param indices A vector of indices of the cards to be discarded
			 */
			void discard(Pile& pile, const std::vector<unsigned>& indices) {
				for (size_t i = 0; i < indices.size(); i++) {
					pile.add(cards.at(indices.at(i)));
				}
			}

			/**
			 * Discards the entire hand on to a pile of cards. Since cards don't
			 * disappear we can't just remove them.
			 *
			 * @param pile A pile of cards on which to put the discarded cards
			 */
			void discard(Pile& pile) {
				while (!isEmpty()) {
					pile.add(Pile::draw());
				}
			}

			/**
			 * Draws a number of cards from a given pile of cards and adds them
			 * to the hand.
			 *
			 * @param pile     A pile of cards from which to draw cards
			 * @param quantity The number of cards to be drawn
			 */
			void draw(Pile& pile, unsigned quantity = 1) {
				for (size_t i = 0; i < quantity; i++) {
					add(pile.draw());
				}
			}

			/**
			 * Returns the name of the hand (player).
			 *
			 * @return The name of the hand
			 */
			std::string getName() const {
				return name;
			}
	};

	/**
	 * Used to print the name of the hand and the string representation of the
	 * cards in it using streams.
	 */
	std::ostream& operator<<(std::ostream& os, const Hand& hand) {
		return os << hand.getName() << ": " << (Pile) hand;
	}
}
