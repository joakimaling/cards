#pragma once

#include <ostream>
#include <string>

namespace cards {
	enum Rank { ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};
	enum Suit { SPADES, CLUBS, HEARTS, DIAMONDS };

	const std::string rankNames[] = { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
	const std::string suitNames[] = { "Spades", "Clubs", "Hearts", "Diamonds" };
	const std::string ranks[] = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K" };
	const std::string suits[] = { "♠", "♣", "♥", "♦" };

	class Card {
		private:
			unsigned rank, suit;
			bool faceUp = false;

		public:
			Card(const unsigned cardNumber): Card(cardNumber % 13, cardNumber / 13) {}

			Card(const Rank rank, const Suit suit): Card((unsigned) rank, (unsigned) suit) {}

			Card(const unsigned rank, const unsigned suit): rank(rank), suit(suit) {}

			Card(const Card& card): Card(card.getRank(), card.getSuit()) {}

			/**
			 * Flips a card. If it's face up its rank and suit are visible when
			 * printed. If face down placeholders are printed in their place.
			 */
			void flip() {
				faceUp = !faceUp;
			}

			/**
			 * Creates and returns a Joker card. This card has no rank, suit or
			 * value, but can be flipped and printed.
			 *
			 * @return The joker card
			 */
			//static Card getJoker() const {
			//	return Card(0, 0);
			//}

			/**
			 * Returns the rank of the card.
			 *
			 * @return The rank of the card
			 */
			unsigned getRank() const {
				return rank;
			}

			/**
			 * Returns the suit of the card.
			 *
			 * @return The suit of the card
			 */
			unsigned getSuit() const {
				return suit;
			}

			/**
			 * Returns the value of the card. Which is the rank incremented by
			 * one, except clad cards having the value of ten, and aces having
			 * the value of 11. Or zero if the card is face down.
			 *
			 * @return The value of the card
			 */
			unsigned getValue() const {
				return faceUp ? (rank == 0 ? 11 : (rank > 9 ? 10 : rank + 1)) : 0;
			}

			/**
			 * Returns true if the card has a red suit, e.i. diamonds or hearts.
			 *
			 * @return True if the card has a red suit
			 */
			bool isRed() const {
				return suit == DIAMONDS || suit == HEARTS;
			}

			/**
			 * Returns true if a card has given rank and suit, false otherwise.
			 *
			 * @param  rank The rank of the card
			 * @param  suit The suit of the card
			 * @return      True if matching, false if not
			 */
			bool is(const unsigned rank, const unsigned suit) const {
				return rank == getRank() && suit == getSuit();
			}

			/**
			 * Returns a string representation of the card. Useful for debugging
			 * or text-based games.
			 *
			 * @return The card as a string
			 */
			std::string toString() const {
				return faceUp ? suits[suit] + ranks[rank] : "▒▒";
			}
	};

	/**
	 * Used to print the string representation of the card using streams.
	 */
	std::ostream& operator<<(std::ostream& os, const Card& card) {
		return os << card.toString();
	}

	/**
	 * Allows to easily check if two cards are equal.
	 */
	bool operator==(const Card& a, const Card& b) {
		return a.getRank() == b.getRank() && a.getSuit() == b.getSuit();
	}

	/**
	 * Allows to easily check if two cards are not equal.
	 */
	bool operator!=(const Card& a, const Card& b) {
		return !(a == b);
	}

	/**
	 * Allows to easily check if the first card is smaller than the second.
	 */
	bool operator<(const Card& a, const Card& b) {
		return a.getRank() < b.getRank();
	}

	/**
	 * Allows to easily check if the first card is greater than the second.
	 */
	bool operator>(const Card& a, const Card& b) {
		return a.getRank() > b.getRank();
	}
}
