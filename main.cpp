#include <allegro5/allegro_native_dialog.h>
#include "games/patience.hpp"

int main() {
	try {
		cards::Patience patience;
		patience.run();
	}
	catch (const std::runtime_error& e) {
		al_show_native_message_box(NULL, "Error", "Error", e.what(), NULL, 2);
	}

	return 0;
}
