CXXFLAGS=-Wall -pedantic -std=c++11
LDFLAGS=`pkg-config --libs allegro{,_dialog,_image}-5`

.PHONY: clean

main: main.o
	$(CXX) $(LDFLAGS) $^ -o $@

main.o: main.cpp cards/*.hpp games/*.hpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	$(RM) main *.o
