#pragma once

#include "../cards/card.hpp"
#include "../cards/deck.hpp"
#include "../cards/pile.hpp"
#include <chrono>
#include <deque>
#include <iostream>
#include <thread>

namespace cards {
	class Klondike {
		private:
			std::deque<Pile> foundation, layout;
			Pile stock, waste;
			Deck deck;
			char a, b;

		public:
			Klondike() {
				deck.shuffle();

				foundation.resize(4);
				layout.resize(7);

				for (std::size_t i = 0, size = layout.size(); i < 8; i++) {
					for (std::size_t j = 0; j < size; j++) {
						layout.at(j).add(deck.draw());

						if (j == size - 1) {
							layout.at(j).flip(layout.at(j).getSize() - 1);
						}
					}

					size--;
				}

				deck.move(stock);
			}

			void fromStockToWaste() {
				try {
					Card card = stock.draw();
					card.flip();
					waste.add(card);
				}
				catch (const std::length_error& e) {
					waste.move(stock);
					stock.flip();
				}
			}

			void fromWasteToLayout(const unsigned index) {
				try {
					layout.at(index).add(waste.draw());
				}
				catch (const std::length_error& e) {
					std::cout << e.what() << std::endl;
				}
			}

			void print() const {
				std::cout << "Stock: " << stock << std::endl;
				std::cout << "Waste: " << waste << std::endl;
				std::cout << std::endl;

				std::cout << "Foundation:" << std::endl;
				for (std::size_t i = 0, size = foundation.size(); i < size; i++) {
					std::cout << i + 1 << ": " << foundation.at(i) << std::endl;
				}

				std::cout << "Layout:" << std::endl;
				for (std::size_t i = 0, size = layout.size(); i < size; i++) {
					std::cout << (char)(i + 65) << ": " << layout.at(i) << std::endl;
				}
			}

			void run() {
				while (true) {
					std::this_thread::sleep_for(std::chrono::milliseconds(100));
					print();
					std::cout << std::endl << "> ";
					std::cin >> a >> b;

					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

					if (a == 'S' && b == 'W') {
						fromStockToWaste();
					}

					std::array<char, 7> letters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };

					if (a == 'W' && std::find(std::begin(letters), std::end(letters), b)) {
						fromWasteToLayout((unsigned) b - 65);
					}
				}
			}
	};
}
