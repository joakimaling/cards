#pragma once

#include "../cards/card.hpp"
#include "../cards/pile.hpp"
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>

namespace cards {
	class MyCard: public Card {
		public:
			using Card::Card;

			void draw(ALLEGRO_BITMAP* bitmap, const int x, const int y) {
				al_draw_bitmap_region(
					bitmap,
					getRank() * (al_get_bitmap_width(bitmap) / 14),
					getSuit() * (al_get_bitmap_height(bitmap) / 4),
					(al_get_bitmap_width(bitmap) / 14),
					(al_get_bitmap_height(bitmap) / 4),
					x,
					y,
					0
				);
			}
	};

	class MyPile: public Pile {
		public:
			void draw(ALLEGRO_BITMAP* bitmap, unsigned x, unsigned y) {
				for (Card& card: cards) {
					reinterpret_cast<MyCard&>(card).draw(bitmap, x, y += 10);
				}
			}
	};

	class Patience {
		private:
			ALLEGRO_DISPLAY* display = nullptr;
			ALLEGRO_EVENT_QUEUE* eventQueue;
			ALLEGRO_TIMER* timer;

			ALLEGRO_BITMAP* bitmap;

			bool isRunning = true;
			bool redraw = false;

			void halt(const std::string& message) {
				al_show_native_message_box(display, "Error", "Error", message.c_str(), NULL, ALLEGRO_MESSAGEBOX_ERROR);
				exit(EXIT_FAILURE);
			}

		public:
			Patience() {
				if (!al_init()) {
					halt("Couln't initialise Allegro");
				}

				if (!(display = al_create_display(800, 600))) {
					halt("Couldn't create a display");
				}

				eventQueue = al_create_event_queue();
				timer = al_create_timer(1.0 / 60.0);

				if (!al_install_mouse()) {
					halt("Couldn't install a mouse");
				}

				al_register_event_source(eventQueue, al_get_display_event_source(display));
				al_register_event_source(eventQueue, al_get_timer_event_source(timer));
				al_register_event_source(eventQueue, al_get_mouse_event_source());

				al_init_image_addon();

				if (!(bitmap = al_load_bitmap("graphics/cards0.bmp"))) {
					halt("Couldn't find the image of cards");
				}
			}

			~Patience() {
				al_destroy_event_queue(eventQueue);
				al_destroy_display(display);
				al_destroy_timer(timer);
			}

			void run() {
				ALLEGRO_EVENT event;

				al_start_timer(timer);

				MyPile pile;

				for (size_t i = 0; i < 4; i++) {
					pile.putTop(MyCard(i, SPADES));
				}

				while (isRunning) {
					al_wait_for_event(eventQueue, &event);

					switch (event.type) {
						case ALLEGRO_EVENT_DISPLAY_CLOSE:
							isRunning = false;
							break;
						case ALLEGRO_EVENT_MOUSE_AXES:
							// see where the mouse is
							break;
						case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
							// grab a card
							break;
						case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
							// drop a card
							break;
						case ALLEGRO_EVENT_TIMER:
							redraw = true;
							break;
					}

					if (redraw && al_is_event_queue_empty(eventQueue)) {
						redraw = false;

						pile.draw(bitmap, 100, 0);

						al_flip_display();
						al_clear_to_color(al_map_rgb(0, 128, 0));
					}
				}
			}
	};
}
