#pragma once

#include "../cards/card.hpp"
#include "../cards/deck.hpp"
#include "../cards/pile.hpp"
#include <chrono>
#include <deque>
#include <iostream>
#include <thread>

namespace cards {
	class GameHand: public Hand {
		public:
			bool hasAce() {
				for (const Card& card: cards) {
					if (card.getRank() == Rank::ACE) {
						return true;
					}
				}

				return false;
			}

			unsigned getTotal() {
				unsigned total = 0;

				for (const Card& card: cards) {
					total += card.getValue();
				}

				if (total <= 11 && hasAce()) {
					total += 10
				}

				return total;
			}
	};

	class House: public GameHand {
		public:
			House(): Hand("House") {}
	}

	class Player: public GameHand {
		public:
			Player(): Hand("Player") {}
	}

	class BlackJack {
		private:
			Deck deck;
			House house;
			Player player;

		public:
			BlackJack() {
				deck.shuffle();

				deck.deal();

			}


	};
}
