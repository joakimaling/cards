#pragma once

#include "../cards/deck.hpp"
#include "../cards/hand.hpp"

namespace cards {
	class Player: public Hand {
		public:
			bool hasRoyalFlush() const {

			}

			bool hasStraight() const {

			}

			bool hasFlush() const {

			}

			bool hasFullHouse() const {

			}

			bool hasFourOfAKind() const {

			}

			bool hasThreeOfAKind() const {

			}

			bool hasTwoPairs() const {

			}

			bool hasOnePair() const {

			}
	}

	class Poker {
		private:
			std::vector<Hand> hands;
			Deck deck;

		public:
			Poker() {
				deck.shuffle();
				deck.deal(hands, HAND_SIZE);
			}


	};
}
